/*
 * main.c
 *
 *  Created on: 22 июл. 2022 г.
 *      Author: d.sovkov
 */

#include <stdint.h>
#include "at32f403a_407.h"
#include "at32f403a_407_gpio.h"
#include "at32f403a_407_crm.h"

void system_clock_config(void)
{
  /* reset crm */
  crm_reset();

  crm_clock_source_enable(CRM_CLOCK_SOURCE_HEXT, TRUE);

   /* wait till hext is ready */
  while(crm_hext_stable_wait() == ERROR)
  {
  }

  /* config pll clock resource */
  crm_pll_config(CRM_PLL_SOURCE_HEXT_DIV, CRM_PLL_MULT_60, CRM_PLL_OUTPUT_RANGE_GT72MHZ);

  /* config hext division */
  crm_hext_clock_div_set(CRM_HEXT_DIV_2);

  /* enable pll */
  crm_clock_source_enable(CRM_CLOCK_SOURCE_PLL, TRUE);

  /* wait till pll is ready */
  while(crm_flag_get(CRM_PLL_STABLE_FLAG) != SET)
  {
  }

  /* config ahbclk */
  crm_ahb_div_set(CRM_AHB_DIV_1);

  /* config apb2clk */
  crm_apb2_div_set(CRM_APB2_DIV_2);

  /* config apb1clk */
  crm_apb1_div_set(CRM_APB1_DIV_2);

  /* enable auto step mode */
  crm_auto_step_mode_enable(TRUE);

  /* select pll as system clock source */
  crm_sysclk_switch(CRM_SCLK_PLL);

  /* wait till pll is used as system clock source */
  while(crm_sysclk_switch_status_get() != CRM_SCLK_PLL)
  {
  }

  /* disable auto step mode */
  crm_auto_step_mode_enable(FALSE);

  /* update system_core_clock global variable */
  system_core_clock_update();
}


void stupid_delay(void)
{
	uint16_t i,j;
	for(i=0;i<1000;i++)
		for(j=0;j<10000;j++);
}

int main(void)
{
	system_clock_config();
	system_core_clock_update();

	crm_periph_clock_enable(CRM_GPIOC_PERIPH_CLOCK, TRUE);

	gpio_init_type GPIO_Init;

	GPIO_Init.gpio_mode = GPIO_MODE_OUTPUT;
	GPIO_Init.gpio_out_type = GPIO_OUTPUT_PUSH_PULL;
	GPIO_Init.gpio_pull = GPIO_PULL_NONE;
	GPIO_Init.gpio_pins = GPIO_PINS_13;
	GPIO_Init.gpio_drive_strength = GPIO_DRIVE_STRENGTH_STRONGER;
	gpio_init(GPIOC, &GPIO_Init);

	while(1)
	{
		gpio_bits_reset(GPIOC, GPIO_PINS_13);
		stupid_delay();
		gpio_bits_set(GPIOC, GPIO_PINS_13);
		stupid_delay();
	}
}
